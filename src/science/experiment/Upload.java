package science.experiment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

public class Upload extends Thread  {

    private volatile boolean stop = false;
    public McrScienceExperimentService experimentService;
    
   
    public Upload(McrScienceExperimentService mcrScienceExperiment) {
    	 experimentService = mcrScienceExperiment;
	}
    
    /**
     *  During a successful run, each data category and data from previous scans is uploaded separately.
     *  Obviously this is not good but is left over from when I was choosing things to upload and setting the formats.
     *  Needs compression.
     */

	@Override
	public void run() {
	      while (!stop) {    	  
	  			String myUsername = experimentService.getMyUsername();	
	  			String data = "";
	  			
	  			if(experimentService.getOnline()) {
	  				System.out.println("ONLINE: ATTEMPTING TO UPLOAD\n\nWITH INTERVAL " + experimentService.getUpdateInterval());
	  				System.out.println("USERNAME: "+myUsername);

	  				// We're online so try to upload everything
			  		try {

			  			// FOR EACH DATA TYPE
			  			for (DataTypes TYPE : DataTypes.values()) {
			  				Log.i("McrScienceExperiment", "Doing uploads for "+TYPE.toString());
				               
			  				data = experimentService.getCachedData(TYPE);
				  			if(data.length() > 0) {
				  				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
					  			nameValuePairs.add(new BasicNameValuePair("me", myUsername  ));
					  			nameValuePairs.add(new BasicNameValuePair("type", TYPE.toString())); 
				  				nameValuePairs.add(new BasicNameValuePair("data", data ));  
						  		
				  				 Log.i("McrScienceExperiment", "Uploading file cache: "+TYPE.toString());
			                       
				  				String uploadStatus = secureconnect(McrScienceExperimentService.uploadURL, nameValuePairs);
						  		if(uploadStatus.replaceAll("\\s", "").compareTo("OK") == 0) {
						  			//System.out.println("UPLOAD OK");
						  			// delete the cache now
						  			experimentService.deleteCachedData(TYPE);
						  			Log.i("McrScienceExperiment", "Upload OK: "+TYPE.toString());
						  		}
						  		else {
						  			//System.out.println("UPLOAD FAIL");
						  			// no need to delete the cache as its already cached
						  		}
				  			}
				  			
				  			data = experimentService.getTempData(TYPE);
				  			if(data.length() > 0) {
					  			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
					  			nameValuePairs.add(new BasicNameValuePair("me", myUsername  ));
					  			nameValuePairs.add(new BasicNameValuePair("type", TYPE.toString()));
				  				nameValuePairs.add(new BasicNameValuePair("data", data ));  
				  				
				  				Log.i("McrScienceExperiment", "Uploading temp cache "+TYPE.toString());
				                    
				  				
						  		String uploadStatus = secureconnect(McrScienceExperimentService.uploadURL, nameValuePairs);
						  		if(uploadStatus.replaceAll("\\s", "").compareTo("OK") == 0) {
						  			Log.i("McrScienceExperiment", "Upload OK: "+TYPE.toString());
						  		}
						  		else {
						  			boolean saved = experimentService.addCacheData(TYPE, data);
						  			if(!saved) // try again later if we cannot cache data
						  				experimentService.addTempData(data, TYPE);
						  		}
				  			}
			  				
			  			}
			  			
					} 
			  		catch (UnsupportedEncodingException e1) {
						//System.out.println("Can't Connect");
					}
	  			}
	  			else {
	  				// We're not online so just cache data
		  			data = experimentService.getTempData(DataTypes.BLUETOOTH_DATA);
	  				experimentService.addCacheData(DataTypes.BLUETOOTH_DATA, data);
		  			data = experimentService.getTempData(DataTypes.WIFI_DATA);
	  				experimentService.addCacheData(DataTypes.WIFI_DATA, data);
		  			data = experimentService.getTempData(DataTypes.GENERIC_DATA);
	  				experimentService.addCacheData(DataTypes.GENERIC_DATA, data);
		  			data = experimentService.getTempData(DataTypes.SENSOR_DATA);
	  				experimentService.addCacheData(DataTypes.SENSOR_DATA, data);
	  			}
	  			
	            try {
					Thread.sleep(experimentService.getUpdateInterval());
				} catch (InterruptedException e) {
					//e.printStackTrace();
				}
	      }
    }

    public synchronized void requestStop() {
            stop = true;
            // nothing as yet but we may want to interrupt threads in the future.
    }
    
    private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			//e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}

		return sb.toString();
	}

    

    /* This is a test function which will connects to a given
	 * rest service and prints it's response
	 */
	public String secureconnect(String url, List<NameValuePair> nameValuePairs) throws UnsupportedEncodingException
	{
		// Instantiate the custom HttpClient
	    DefaultHttpClient httpclient = this.new MyHttpClient();

	    // Prepare a request object
 		HttpPost httppost = new HttpPost(url);
 		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));  
 		
 		// Execute the request
 		HttpResponse response;
 		try {
 			response = httpclient.execute(httppost);
 			// Examine the response status
 			//System.out.println("Resonse: " + response.getStatusLine().toString());

 			// Get hold of the response entity
 			HttpEntity entity = response.getEntity();
 			// If the response does not enclose an entity, there is no need
 			// to worry about connection release

 			if (entity != null) {
 				  
 				// A Simple JSON Response Read
 				InputStream instream = entity.getContent();
 				String result= convertStreamToString(instream);
 				//System.out.println("Response text: " + result);
 				
 				// Closing the input stream will trigger connection release
 				instream.close();
 				  
 				return result;
 			}


 		} catch (ClientProtocolException e) {
			Log.e("McrScienceExperiment", "Uploading error 1:"+e.getMessage());
 			//e.printStackTrace();
 		} catch (IOException e) {
			Log.e("McrScienceExperiment", "Uploading error 2:", e);
 			//e.printStackTrace();
 		}
 		return "ERROR";
	}
    
	
    
 
    
    /* This is a test function which will connects to a given
	 * rest service and prints it's response
	 */
	/*
	public static String connect(String url, List<NameValuePair> nameValuePairs) throws UnsupportedEncodingException
	{

		HttpClient httpclient = new DefaultHttpClient();
			
		//System.out.println("Hammers: " + nameValuePairs.toString());
		
		// Prepare a request object
		HttpPost httppost = new HttpPost(url);
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));  
		
		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(httppost);
			// Examine the response status
			//System.out.println("Resonse: " + response.getStatusLine().toString());

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result= convertStreamToString(instream);
				//System.out.println("Response text: " + result);
				
				// Closing the input stream will trigger connection release
				instream.close();
				return result;
			}


		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ERROR";
	}
	*/
	
	private class MyHttpClient extends DefaultHttpClient {

		public MyHttpClient() {

	    }
	 
	    @Override
	    protected ClientConnectionManager createClientConnectionManager() {
	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	        // Register for port 443 our SSLSocketFactory with our keystore
	        // to the ConnectionManager
	        registry.register(new Scheme("https", newSslSocketFactory(), 443));
	        return new SingleClientConnManager(getParams(), registry);
	    }
	 
	    private SocketFactory newSslSocketFactory() {
	        try {
	        	
	            // Get an instance of the Bouncy Castle KeyStore format
	            KeyStore trusted = KeyStore.getInstance("BKS");
	            // Get the raw resource, which contains the keystore with
	            // your trusted certificates (root and any intermediate certs)
	            InputStream in = experimentService.getApplicationContext().getResources().openRawResource(R.raw.mykeystore);
	            try {
	                // Initialize the keystore with the provided trusted certificates
	                // Also provide the password of the keystore
	                trusted.load(in, "secure".toCharArray());
	            } finally {
	                in.close();
	            }
	            // Pass the keystore to the SSLSocketFactory. The factory is responsible
	            // for the verification of the server certificate.
	            SSLSocketFactory sf = new SSLSocketFactory(trusted);
	            // Hostname verification from certificate
	            // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
	            sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
	            return sf;
	        } catch (Exception e) {
	        	Log.i("McrScienceExperiment", "UPLOADin error 3:"+e.getMessage());	 			
	            throw new AssertionError(e);
	        }
	    }
	}
    
	
}
