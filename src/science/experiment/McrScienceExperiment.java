package science.experiment;



import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class McrScienceExperiment extends Activity {
	    
	/**
	 * Tag used for log messages
	 */
	private static final String TAG = "McrScienceExperiment";
	
    // Member fields
	private Button hideButton;
	private Button askButton;
	private BluetoothAdapter mBtAdapter;
    
    /**
     * global/important preferences
     */
    public static final String PREFS_NAME = "MyPrefsFile";
   	
    /**
     * Text used for Go on/off-line button
     */
	private static final String GO_ONLINE = "Go online";
	private static final String GO_OFFLINE = "Go offline";
	private String onLineText;

    /**
     * Text used for Stop/start scanning button
     */
	private static final String START_SCANNING = "Start scanning";
	private static final String STOP_SCANNING = "Stop scanning";
	private String scanningText;
	
	/** 
	 * Bluetooth MAC address. This is used as the username of the device when uploading data
	 */
	private String bluetoothAddr;
	
	
	protected Editable value;

    

	private McrScienceExperimentService mBoundService;

	
    @Override
    protected void onStop(){
       super.onStop();
       Log.i("McrScienceExperiment", "Activity stopped");
    }
    
	@Override
	public void onBackPressed() {
		Log.i("McrScienceExperiment", "Activity back pressed");

	    // do default on back.
		moveTaskToBack(true);
	}
	
	/**
	 * Points to the experiment service. Binds and unbinds service when scanning
	 * 
	 * @see startScanning()
	 * @see stopService()
	 */
    private ServiceConnection mConnection = new ServiceConnection() {
    	
    	/**
    	 * This is called when the connection with the service has been
    	 * established, giving us the service object we can use to
    	 * interact with the service.  Because we have bound to a explicit
    	 * service that we know is running in our own process, we can
    	 * cast its IBinder to a concrete class and directly access it.
    	 */
    	@Override
		public void onServiceConnected(ComponentName className, IBinder service) {

        	//System.out.println("@!!!!!!!!!!!!!!!!!!!!!!!! SERVICE BOUND");
            mBoundService = ((McrScienceExperimentService.LocalBinder)service).getService();
            //startService(mBoundService);
            /*
            if(bluetoothAddr != null) {
            	mBoundService.setBluetoothAddress(bluetoothAddr);
            }
            */
        }

    	/**
    	 * This is called when the connection with the service has been
    	 * unexpectedly disconnected -- that is, its process crashed.
    	 * 
    	 * Because it is running in our same process, we should never
    	 * see this happen.
    	 * 
    	 * TODO Shouldn't we do something if this DOES happen then?
    	 */
        @Override
		public void onServiceDisconnected(ComponentName className) {
        	stopScanning();
        }

        
    };

    /**
     * Stops the currently running service
     */
	private void stopService() {
		if(mBoundService != null) {
			//mBoundService.stopScanning();
			try {
				unbindService(mConnection);
				mBoundService = null;
			}
			catch(IllegalArgumentException e) {
				// mConnection already disconnected
			}
		}
		stopService(new Intent(this, McrScienceExperimentService.class));
	}
	

	
    
    
    /** 
     * Called when the activity is first created. If bluetooth is not available 
     * the application will alert the user and close
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("McrScienceExperiment", "Activity created");
 
        try {
	        // Get the local Bluetooth adapter
	        mBtAdapter = BluetoothAdapter.getDefaultAdapter();	        
	        
	        // If the adapter is null, then Bluetooth is not supported. Force application to quit
	        if (mBtAdapter == null) {
	        	//TODO Make this message a bit more helpful to users?	        	
	            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
	            
	            //Force quit application
	            finish();
	            return;
	        }
	        
	        //wait until bluetooth has enabled
	        //TODO could this ever get stuck?
	        boolean enabling = false;
	        while (!mBtAdapter.isEnabled()) {
	            //Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	            //startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
	        	if(!enabling) {
	        		//TODO this returns instantly, need to listen for ACTION_STATE_CHANGED as per documentation
	        		mBtAdapter.enable();	
	        		enabling = true;
	        	}
	        }
	        
	        bluetoothAddr = BluetoothAdapter.getDefaultAdapter().getAddress();
	        //System.out.println("Bluetooth Address #1: " + bluetoothAddr);
	    }
        catch (Exception e) {
        	// Lots of exceptions can be called if Bluetooth is buggered/not available
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            Log.e(TAG,e.toString());
            finish();
        }
    }

    
	
    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "Activity started");
        setupGui();
        
        //Get preference file
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

		// register BT address as username
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("username", bluetoothAddr);
    	editor.commit();	
        
        // Check if user has accepted the disclaimer
        boolean disclaimer = settings.getBoolean("disclaimer", false);
        Log.i(TAG, "Disclaimer check :"+disclaimer);
        
        //If user hasn't already accepted the disclaimer, show it now
        if(!disclaimer) {
        	showDisclaimerDialog();
        }
        else {
        	startExperiment();
        }
    }    
    
    /**
     * Shows the disclaimer to the user in an AlertDialog box. If the user agrees
     * the application proceeds. If not then it closes.
     */
    private void showDisclaimerDialog()
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setCancelable(true);
    	builder.setTitle("Disclaimer");
    	builder.setMessage(R.string.disclaimer);
    	builder.setInverseBackgroundForced(true);
    	
    	//Agree button
    	builder.setPositiveButton("Agree", new DialogInterface.OnClickListener() {
	    	@Override
			public void onClick(DialogInterface dialog, int which) {
	    		
	    	      SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    	      SharedPreferences.Editor editor = settings.edit();
	    	      editor.putBoolean("disclaimer", true);
	    	      // Commit the edits!
	    	      editor.commit();
	    	      startExperiment();
	    	      
	    	      dialog.dismiss();
	    	}
	    });
    	
    	//Close (disagree) button
	    builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
	    	@Override
			public void onClick(DialogInterface dialog, int which) {
	    	   dialog.dismiss();
	    	   finish();
	    	}
    	});
	    
    	AlertDialog alert = builder.create();
    	alert.show();
    }
    
    @Override
    protected void onPause() {
        //gpsStopListening();
        super.onPause();
        Log.i("McrScienceExperiment", "Activity Paused");

    }

    @Override
    protected void onResume() {
        //gpsStartListening();
        super.onResume();
        Log.i("McrScienceExperiment", "Activity resumed");

    }
    
    @Override
    protected void onDestroy() {
    	//gpsStopListening();
        super.onDestroy();
        this.stopScanning();
        /* SERVICE TAKE OUT
        if(mBoundService != null)
        	mBoundService.stopScanning();
        */
        //stopScanningServices();
        // Unregister broadcast listeners
        //this.unregisterReceiver(mReceiver);
        Log.i("McrScienceExperiment", "Activity destroyed");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    /**
     * These are the actions to do when menu items have been pressed
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    
    	//Each item in the menu list
    	switch (item.getItemId()) 
    	{
    		//Start new scan or cancel current if already scanning
	        case R.id.scan:
	        	if(settings.getBoolean("scanning", false))
	        		this.stopScanning();
	    	    else
	    	    	this.startScanning();
	            return true;
	            
            //Change between online and offline modes
	        case R.id.online:
	        	if(settings.getBoolean("online", false))
	        		this.stopUpload();
	    	    else
	    	    	this.startUpload();
	        	return true;
	        	
    		//Set the upload interval time
	        case R.id.interval:
	        	showUploadIntervalDialog();
	        	return true;      
	        	
	    }//end switch
        return false;
    }
    
    private void showUploadIntervalDialog()
    {
    	// display update sending interval dialog
		final String items[] = {"5 Minutes","10 Minutes","30 Minutes","1 Hour","3 Hours","Day"};
		AlertDialog.Builder ab=new AlertDialog.Builder(McrScienceExperiment.this);
		ab.setTitle("Send data every ...");
		ab.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface d, int choice) {
				// on Cancel button action
			}
		});				
		ab.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface d, int choice) {
				// 60 000 miliseconds in a minute
				long interval = 300000;
				if(choice == 0) {
					interval = 300000;
				}
				else if(choice == 1) {
					interval = 600000;
				}
				else if(choice == 2) {
					interval = 1800000;
				}
				else if(choice == 3) {
					interval = 3600000;
				}
				else if(choice == 4) {
					interval = 10800000;
				}
				else if(choice == 5) {
					interval = 86400000;
				}
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    	    SharedPreferences.Editor editor = settings.edit();
	    	    editor.putLong("interval", interval);
	    	    // Commit the edits!
	    	    editor.commit(); 
			}
		});
		ab.show();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      setContentView(R.layout.main);
      setupGui();
    }

    
    
    /*
     * GUI
     */
    
    /**
     * Called before menu is shown. Labels are changed depending on 
     * current state of the application. e.g. If already scanning
     * 'stop scan' is shown.
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //Show text for on/off-line button
		MenuItem onlineButton = menu.findItem(R.id.online);
	    onlineButton.setTitle(onLineText);

 		//Show text for scan on/off button
		MenuItem scanningButton = menu.findItem(R.id.scan);
		scanningButton.setTitle(scanningText);
		
        return true;
    }
    
    
    private void setupGui() {
        setContentView(R.layout.main);
      
        // Set result CANCELED incase the user backs out
        setResult(Activity.RESULT_CANCELED);
		
		//Attach button click handlers
		hideButton = (Button)findViewById(R.id.button_hide);
		hideButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				  moveTaskToBack(true);
			}
		});
		
		askButton = (Button)findViewById(R.id.button_ask);
		askButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent myIntent = new Intent(McrScienceExperiment.this, QuestionActivity.class);
				McrScienceExperiment.this.startActivity(myIntent);
			}
		});
		
		Button uploadButton = (Button)findViewById(R.id.button_upload);
		uploadButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Log.i(TAG,"Upload button pressed");
			}
		});
		
		Button cacheButton = (Button)findViewById(R.id.button_cache);
		cacheButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Log.i(TAG,"Cache button pressed");
			}
		});
		
		
		//Correct text that changes
	    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	 
	    TextView scanningTextBox = (TextView) findViewById(R.id.textView4);
	    if(settings.getBoolean("scanning", false)) {
	    	scanningTextBox.setText("On");
	    	scanningTextBox.setTextColor(Color.parseColor("#00FF00"));
	    	scanningText = STOP_SCANNING;
		}
	    else {
	    	scanningTextBox.setText("Off");
	    	scanningTextBox.setTextColor(Color.parseColor("#FF0000"));
	    	scanningText = START_SCANNING;
	    }
	    
	    TextView onlineTextBox = (TextView) findViewById(R.id.textView5);
	    if(settings.getBoolean("online", false)) {
	    	onlineTextBox.setText("Online");
	    	onlineTextBox.setTextColor(Color.parseColor("#00FF00"));
	    	onLineText = GO_OFFLINE;
	    }	    
	    else {
	    	onlineTextBox.setText("Offline");
	    	onlineTextBox.setTextColor(Color.parseColor("#FF0000"));
	    	onLineText = GO_ONLINE;
	    }
	    
	    TextView intervalTextBox = (TextView) findViewById(R.id.intervalText);
	    intervalTextBox.setText(settings.getLong("interval", (long)0)+"ms");
	    
    }
    
    /*
     * Experiment functions
     */
    
    
	public void startExperiment() {
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		
		//Double check disclaimer has been accepted
        boolean disclaimer = settings.getBoolean("disclaimer", false);
        if(disclaimer) {
			 // on start check if scanning and uploading if we're supposed to
	        // check if scanning is allowed
	        boolean scanning = settings.getBoolean("scanning", false);
	        
	        if(scanning && mBoundService == null) {
	        	startScanning();
	        }

	        //If scanning is not enabled, show dialog to turn it on
	        if(!scanning) {
	        	showScanningDialog();
	        }
	        
	        // Offer choice of online or offline mode
	        boolean online = settings.getBoolean("online", false);
	        if(online) {
	        	startUpload();
	        }
	        else {
	        	showUploadDialog();
	        }
        }
        else
        {
        	Log.e(TAG, "Somehow startExperiment() has been called without accepting disclaimer");
        }
	}
	
	/**
	 * Dialog asking user to enable scanning. A scan is done after pressing ok
	 */
	private void showScanningDialog()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setCancelable(true);
    	builder.setTitle("Start scanning?");
    	builder.setMessage(R.string.scanrequest);
    	builder.setInverseBackgroundForced(true);
    	builder.setPositiveButton("Enable scanning", new DialogInterface.OnClickListener() {
	    	@Override
			public void onClick(DialogInterface dialog, int which) {
	    	      startScanning();
	    	      dialog.dismiss();
	    	}
	    });
	    builder.setNegativeButton("Keep scanning turned off", new DialogInterface.OnClickListener() {
	    	@Override
			public void onClick(DialogInterface dialog, int which) {
	    	      dialog.dismiss();
	    	}
    	});
    	AlertDialog alert = builder.create();
    	alert.show();
	}

	/**
	 * Dialog asking user to enable uploading. Existing data is uploaded after pressing ok
	 */
	private void showUploadDialog()
	{
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setCancelable(true);
    	builder.setTitle("Go online?");
    	builder.setMessage(R.string.onlinequest);
    	builder.setInverseBackgroundForced(true);
    	builder.setPositiveButton("Enable upload", new DialogInterface.OnClickListener() {
	    	@Override
			public void onClick(DialogInterface dialog, int which) {
	    		startUpload();
	    	    dialog.dismiss();
	    	}
	    });
	    builder.setNegativeButton("Stay in offline mode", new DialogInterface.OnClickListener() {
	    	@Override
			public void onClick(DialogInterface dialog, int which) {
	    		dialog.dismiss();
	    	}
    	});
    	AlertDialog alert = builder.create();
    	alert.show();
	}
	
    public void startScanning(){
    	Log.i("McrScienceExperiment", "Request to start scanning");
    	/*
    	// Alarm managers, I'm rethinking this now, alarm managers make it difficult to wait for
    	// scans e.g. location and Bluetooth, when CPU is in sleep mode
    	// Start service using AlarmManager
    	
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        Intent alarmIntent = new Intent(McrScienceExperiment.this, WirelessScanAlarmReceiver.class);
    	pendingIntent = PendingIntent.getBroadcast(McrScienceExperiment.this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    	
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pendingIntent); //cancel if active already
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis()+WIRELESS_SCAN_TICK_RATE, WIRELESS_SCAN_TICK_RATE, pendingIntent);
    	//alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    	*/
    	
		// bind to service
    	stopService(); // stop if already running
		Intent serviceIntent = new Intent(McrScienceExperiment.this, McrScienceExperimentService.class);
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
		this.getApplicationContext().startService(serviceIntent);
		
		
		//Change text for scan option on menu
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
	    editor.putBoolean("scanning", true);
	    editor.commit();
	    
	    scanningText = STOP_SCANNING;
		TextView textBox = (TextView) findViewById(R.id.textView4);
        textBox.setText("On");
        textBox.setTextColor(Color.parseColor("#00FF00"));
    }
    
    public void startUpload(){
    	Log.i("McrScienceExperiment", "request to start upload");
		
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
	    editor.putBoolean("online", true);
	    // Commit the edits!
	    editor.commit();			
		// change the title of the button
	    onLineText = GO_OFFLINE;
        // Set up the custom title
		TextView textBox = (TextView) findViewById(R.id.textView5);
        textBox.setText("Online");
        textBox.setTextColor(Color.parseColor("#00FF00"));
        
    	
		if(mBoundService != null) {
			//System.out.println("INTERVAL: " + interval);
			mBoundService.startUploadOperation();
		}
		
    }
    public void stopScanning(){
    	Log.i("McrScienceExperiment", "request to stop scanning");
		
    	/*** Alarms are tricky with CPU locks, going to try without first
    	AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pendingIntent); //cancel if active already
    	*/
    	
        stopService();

		//setTitle(R.string.scanning_stopped);
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
	    editor.putBoolean("scanning", false);
	    // Commit the edits!
	    editor.commit();
	    scanningText = START_SCANNING;
	    TextView textBox = (TextView) findViewById(R.id.textView4);
        textBox.setText("Off");
        textBox.setTextColor(Color.parseColor("#FF0000"));
    }
    public void stopUpload(){
    	Log.i("McrScienceExperiment", "request to stop upload");
		
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
	    editor.putBoolean("online", false);
	    // Commit the edits!
	    editor.commit();		
    	onLineText = GO_ONLINE;
		TextView textBox = (TextView) findViewById(R.id.textView5);
        textBox.setText("Offline");
        textBox.setTextColor(Color.parseColor("#FF0000"));
        
    	if(mBoundService != null) {
			//System.out.println("INTERVAL: " + interval);
			mBoundService.stopUploadOperation();
		}
    }
    
}