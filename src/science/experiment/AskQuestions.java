package science.experiment;


import java.util.Calendar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;



public class AskQuestions extends Thread  {
	
	private McrScienceExperimentService mcrScience;
	private volatile boolean stop = false;
	private int id = 0;


	public AskQuestions(McrScienceExperimentService mcrScienceExperimentService) {
    	mcrScience = mcrScienceExperimentService;
    	id = 0;
    }
	
	
	
	 @Override
	public void run() {
		while (!stop) {
			
			// only put up a notification if between 8am and 9pm (so not to be annoying)
			Calendar calendar = Calendar.getInstance();
			//calendar.setTime( calendar.getTime() );
			int hours = calendar.get( Calendar.HOUR_OF_DAY );
			//int minutes = calendar.get( Calendar.MINUTE );
			if(hours > 8 && hours < 21) {

				NotificationCompat.Builder mBuilder =
					        new NotificationCompat.Builder(mcrScience.getApplicationContext())
					        .setSmallIcon(R.drawable.mcrwireless)
					        .setContentTitle("New Question")
					        .setContentText("New Question");
				// Creates an explicit intent for an Activity in your app
				Intent questionIntent = new Intent(mcrScience.getApplicationContext(), QuestionActivity.class);
				
				// The stack builder object will contain an artificial back stack for the
				// started Activity.
				// This ensures that navigating backward from the Activity leads out of
				// your application to the Home screen.
				TaskStackBuilder stackBuilder = TaskStackBuilder.create(mcrScience.getApplicationContext());
				// Adds the back stack for the Intent (but not the Intent itself)
				stackBuilder.addParentStack(QuestionActivity.class);
				// Adds the Intent that starts the Activity to the top of the stack
				stackBuilder.addNextIntent(questionIntent);
				PendingIntent resultPendingIntent =
				        stackBuilder.getPendingIntent(
				            0,
				            PendingIntent.FLAG_UPDATE_CURRENT
				        );
				mBuilder.setContentIntent(resultPendingIntent);
				NotificationManager mNotificationManager =
				    (NotificationManager) mcrScience.getSystemService(Context.NOTIFICATION_SERVICE);
				// mId allows you to update the notification later on.
				Notification notification = mBuilder.build();
				notification.flags |= Notification.FLAG_SHOW_LIGHTS;
				notification.ledARGB=0xffffffff; //color, in this case, white
				notification.ledOnMS=1000; //light on in milliseconds
				notification.ledOffMS=4000; //light off in milliseconds
				notification.defaults |= Notification.DEFAULT_SOUND;
				notification.defaults |= Notification.DEFAULT_VIBRATE;
				
				mNotificationManager.notify(id, notification);
			}
				//id++;			 
			 try {
				 Thread.sleep(McrScienceExperimentService.QUESTION_TICK_RATE);
			 }
			 catch (InterruptedException e) {
				 // TODO Auto-generated catch block
			 //e.printStackTrace();
			 }
		 }
		 if (stop) {
			 //  System.out.println("Detected stop");
		 }
     }
	 

     public synchronized void requestStop() {
             stop = true;
     }
     

}
