package science.experiment;

public enum DataTypes {
	BLUETOOTH_DATA,
	WIFI_DATA,
	GENERIC_DATA,
	LOG_DATA,
	SENSOR_DATA,
	QUESTION_DATA
}
