package science.experiment;


public class GenericScanner extends Thread  {
	
	private McrScienceExperimentService mcrScience;
	private volatile boolean stop = false;



	public GenericScanner(McrScienceExperimentService mcrScienceExperimentService) {
    	mcrScience = mcrScienceExperimentService;
    }
	
	 @Override
	public void run() {	
	      while (!stop) {	          
	    	  mcrScience.addTempData(mcrScience.getGenericData(), DataTypes.GENERIC_DATA);
            try {
				Thread.sleep(McrScienceExperimentService.SCIENCE_SCAN_TICK_RATE);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
	      }
	      if (stop) {
	            //  System.out.println("Detected stop");
	      }
     }
	 

     public synchronized void requestStop() {
             stop = true;
     }
     

}
