package science.experiment;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LogGatherer extends Thread  {
	
	private McrScienceExperimentService mcrScience;
	private volatile boolean stop = false;
	//private List<String> wireless;


	public LogGatherer(McrScienceExperimentService mcrScienceExperimentService) {

    	mcrScience = mcrScienceExperimentService;



		// Get WiFi status
		//WifiInfo info = wifi.getConnectionInfo();
		//textStatus.append("\n\nWiFi Status: " + info.toString());
  }
	
	private String getFormattedKernelVersion() 
    {
        String procVersionStr;

        try {
            BufferedReader reader = new BufferedReader(new FileReader("/proc/version"), 256);
            try {
                procVersionStr = reader.readLine();
            } finally {
                reader.close();
            }

            final String PROC_VERSION_REGEX =
                "\\w+\\s+" + /* ignore: Linux */
                "\\w+\\s+" + /* ignore: version */
                "([^\\s]+)\\s+" + /* group 1: 2.6.22-omap1 */
                "\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+" + /* group 2: (xxxxxx@xxxxx.constant) */
                "\\([^)]+\\)\\s+" + /* ignore: (gcc ..) */
                "([^\\s]+)\\s+" + /* group 3: #26 */
                "(?:PREEMPT\\s+)?" + /* ignore: PREEMPT (optional) */
                "(.+)"; /* group 4: date */

            Pattern p = Pattern.compile(PROC_VERSION_REGEX);
            Matcher m = p.matcher(procVersionStr);

            if (!m.matches()) {
                return "Unavailable";
            } else if (m.groupCount() < 4) {
                return "Unavailable";
            } else {
                return (new StringBuilder(m.group(1)).append("\n").append(
                        m.group(2)).append(" ").append(m.group(3)).append("\n")
                        .append(m.group(4))).append("\n\n").toString();
            }
        } catch (IOException e) {  
              return "Unavailable";
        }
    }
	
	@Override
	public void run() {	
	      while (!stop) {
	          /*
		        Usage: logcat [options] [filterspecs]
		        options include:
		          -s              Set default filter to silent.
		                          Like specifying filterspec '*:s'
		          -f <filename>   Log to file. Default to stdout
		          -r [<kbytes>]   Rotate log every kbytes. (16 if unspecified). Requires -f
		          -n <count>      Sets max number of rotated logs to <count>, default 4
		          -v <format>     Sets the log print format, where <format> is one of:
		
		                        * brief � Display priority/tag and PID of originating process (the default format).
							    * process � Display PID only.
							    * tag � Display the priority/tag only.
							    * thread � Display process:thread and priority/tag only.
							    * raw � Display the raw log message, with no other metadata fields.
							    * time � Display the date, invocation time, priority/tag, and PID of the originating process.
							    * long � Display all metadata fields and separate messages with a blank lines.

		
		          -c              clear (flush) the entire log and exit
		          -d              dump the log and then exit (don't block)
		          -g              get the size of the log's ring buffer and exit
		          -b <buffer>     request alternate ring buffer
		                          ('main' (default), 'radio', 'events')
		          -B              output the log in binary
		        filterspecs are a series of
		          <tag>[:priority]
		
		        where <tag> is a log component tag (or * for all) and priority is:
		          V    Verbose
		          D    Debug
		          I    Info
		          W    Warn
		          E    Error
		          F    Fatal
		          S    Silent (supress all output)
		
		        '*' means '*:d' and <tag> by itself means <tag>:v
		
		        If not specified on the commandline, filterspec is set from ANDROID_LOG_TAGS.
		        If no filterspec is found, filter defaults to '*:I'
		
		        If not specified with -v, format is set from ANDROID_PRINTF_LOG
		        or defaults to "brief"
	        */

	       
	        
	        fetchLog();
    	  

            try {
				Thread.sleep(McrScienceExperimentService.SCIENCE_SCAN_TICK_RATE);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
	      }
	      if (stop) {
	            //  System.out.println("Detected stop");
	      }
     }
	 

     public synchronized void requestStop() {
             stop = true;
     }
     

     public void fetchLog(){
         final StringBuilder log = new StringBuilder();
         try{
        	 
        	 ArrayList<String> list = new ArrayList<String>();


 	        
             list.add("logcat");//$NON-NLS-1$
             list.add("-d");//$NON-NLS-1$
  	        list.add("-v");
 	        list.add("long");
 	        //list.add("-b");
 	        //list.add("main");
 	        list.add("*:I");             
             
             Process process = Runtime.getRuntime().exec(list.toArray(new String[0]));
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()),8 * 1024);
             String line;
             while ((line = bufferedReader.readLine()) != null){ 
                 log.append(line);
                 //log.append(App.LINE_SEPARATOR); 
             }
             // once it is done clear the log
             ArrayList<String> commandLine2 = new ArrayList<String>();
             commandLine2.add("logcat");//$NON-NLS-1$
             commandLine2.add("-c");//$NON-NLS-1$
             @SuppressWarnings("unused")
			Process process2 = Runtime.getRuntime().exec(commandLine2.toArray(new String[0]));
             
         } 
         catch (IOException e){
             //Log.e(App.TAG, "CollectLogTask.doInBackground failed", e);//$NON-NLS-1$
        	 e.printStackTrace();
         } 
         catch (Exception e){
             //Log.e(App.TAG, "CollectLogTask.doInBackground failed", e);//$NON-NLS-1$
        	 e.printStackTrace();
         } 

         
         mcrScience.addCacheData( DataTypes.LOG_DATA, getFormattedKernelVersion() + log.toString());
         
     }
	   
	

}
