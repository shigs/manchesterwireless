package science.experiment;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;


/**
 * Logs sensor data.
 * This currently uses the ACCEL enum type even though we lump a load of sensor data in here
 * 
 * @author Matt
 *
 */
public class Sensors extends Thread {
	
	private McrScienceExperimentService mcrScience;
	private volatile boolean stop = false;


	// acceleration
    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    private int i;
    
	public Sensors(McrScienceExperimentService mcrScienceExperimentService) {
    	mcrScience = mcrScienceExperimentService;
    	i = 0;
    }
	
	
	public void run() {	
		
		mSensorManager = (SensorManager) mcrScience.getSystemService(Context.SENSOR_SERVICE);

		// Slow changing sensors light light can go outside the loop
		// light
		mSensorManager.registerListener(lightSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL);
		//mSensorManager.registerListener(lightSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_NORMAL);

    	// The loop controls sensors (like accelerometer) which change too often to be stored properly
		while (!stop) {
			i = 0;
			// acceleration
			//Log.i("ManchesterWireless", "Accel: starting listening");
	    	mSensorManager.registerListener(accelSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
	    	mAccel = 0.00f;
	    	mAccelCurrent = SensorManager.GRAVITY_EARTH;
	    	mAccelLast = SensorManager.GRAVITY_EARTH;
	    	
	    	// other sensors
	    	mSensorManager.registerListener(accelSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_NORMAL);
			//mSensorManager.registerListener(accelSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
			//mSensorManager.registerListener(accelSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE), SensorManager.SENSOR_DELAY_NORMAL);

	        try {
				Thread.sleep(McrScienceExperimentService.SENSOR_SCAN_TIME);
				mSensorManager.unregisterListener(accelSensorListener);
				Thread.sleep(McrScienceExperimentService.SENSOR_SCAN_TICK_RATE-McrScienceExperimentService.SENSOR_SCAN_TIME);
			}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		if (stop) {
			//  System.out.println("Detected stop");
		}	
	}
	 

	public synchronized void requestStop()  {
		
		Log.i("McrScienceExperiment", "request stop of sensors");
		
		
		mSensorManager.unregisterListener(accelSensorListener);
		mSensorManager.unregisterListener(lightSensorListener);
		
		stop = true;
	}
     
	
	// need multiple listeners because some are running in a loop to save disk space
    private final SensorEventListener accelSensorListener = new SensorEventListener() {
    	public void onSensorChanged(SensorEvent se) {
    		
	   		if(se.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {   	
		   		float x = se.values[0];
	    		float y = se.values[1];
	    		float z = se.values[2];
	    		mAccelLast = mAccelCurrent;
	    		mAccelCurrent = android.util.FloatMath.sqrt(x*x + y*y + z*z);
	    		float delta = mAccelCurrent - mAccelLast;
	    		mAccel = mAccel * 0.9f + delta; // perform low-cut filter
	    		//Log.i("ManchesterWireless", "Accel: " + mAccel);
	    		
	    		// first two records are usually junk
	    		if(i > 2)
	    			//mcrScience.addTempData("ACCELEROMETER: "+String.valueOf(mAccel)+",", DataTypes.SENSOR_DATA);
	    			mcrScience.addTempData("ACCELEROMETER: " + x + " " + y + " " + z +",", DataTypes.SENSOR_DATA);
	    		i++;
	    	}
	   		else if(se.sensor.getType() == Sensor.TYPE_ORIENTATION) {
	  	       float x = se.values[0];
	  	       float y = se.values[1];
	  	       float z = se.values[2];
	  	       mcrScience.addTempData("ORIENTATION: " + x + " " + y + " " + z +",", DataTypes.SENSOR_DATA);
	  	       //Log.i("ManchesterWireless", "ORIENTATION: " + x + " " + y + " " + z);
	  		}
	   		//else if(se.sensor.getType() == Sensor.TYPE_TEMPERATURE) {
	      	//       float currentLux = se.values[0];
	      	//        Log.i("ManchesterWireless", "TEMPERATURE: " + currentLux);
	      	//}
	    }
 		

    	public void onAccuracyChanged(Sensor sensor, int accuracy) {
    	}
    };
    

    private final SensorEventListener lightSensorListener = new SensorEventListener() {
    	public void onSensorChanged(SensorEvent se) {
    		
    		if(se.sensor.getType() == Sensor.TYPE_LIGHT) {
    	       float currentLux = se.values[0];
    	        //mcrScience.addData(""+String.valueOf(mAccel)+",", DataTypes.ACCEL_DATA);
    	        //Log.i("ManchesterWireless", "LIGHT: " + currentLux);
    	       mcrScience.addTempData("LIGHT: "+currentLux+",", DataTypes.SENSOR_DATA);

    		}
    		

    	}

    	public void onAccuracyChanged(Sensor sensor, int accuracy) {
    	}
    };
    
    

}
