package science.experiment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.os.BatteryManager;


/****
 * This serice looks after all the threads which do the scanning. Threads? I hear you cry. Yes threads, here's why.
 * The use of alarm managers is better on the CPU but its harder to keep the CPU alive whilst the scanning happens.
 * i.e. its impossible to predict how long a wifi scan will take, so managing the wakelock for individual scans requires lots of extra code.
 * Thus this thread will acquire a wakelock and release it when it is destroyed.
 * 
 * Comments from others saying similar things:
 * "If the polling is supposed to go on even if the activity is not in the foreground, use AlarmManager. However, most recipes for using AlarmManager will have the real work (in your case, GPS fix and network I/O) be handled by an IntentService. This will not work in your case, because GPS is asynchronous -- you cannot just get a fix whenever you feel like it. It will take a long time, possibly forever, to get a fix, so you have to deal with the delay and eventually timing out the operation. Writing a Service to do this is possible, but tricky, particularly if you are aiming to collect this information even if the device falls asleep."
 * http://stackoverflow.com/questions/5766912/android-background-service-and-alarmmanager?rq=1
 * 
 * Also, sensors are turned off with the screen! Wakelock does not help with this on the majority of HTC and Samsung phones, SCREEN_DIM_WAKE_LOCK might.
 * http://stackoverflow.com/questions/2143102/accelerometer-stops-delivering-samples-when-the-screen-is-off-on-droid-nexus-one?rq=1
 * 
 * @author Matt
 *
 */
public class McrScienceExperimentService extends Service implements LocationListener {

    // Return Intent extra
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
	private int SOFTWARE_VERSION;
	private long interval;

	public static final long QUESTION_TICK_RATE = 10800000; // ask question every 3 hours = 10800000
	public static final long SCIENCE_SCAN_TICK_RATE = 120000; // Bluetooth,  "generic", Log, and Wifi scan every 2 minutes
	public static final long SENSOR_SCAN_TICK_RATE = 10000; // SENSORS SCAN EVERY 10 SECONS
	public static final int SENSOR_SCAN_TIME = 2000; // SENSORS SCAN FOR 2 SECONDS
	
	private static final int MAX_TEMP_BUFFER_SIZE = 100; // maximum number of entries in cache before they get written to files
	private static final long GPS_SCAN_INTERVAL = 120000; // 2 minutes
	public static final int WIFI_SCAN_TIME = 15000; // 15 seconds
	public static final int WIFI_START_WAIT = 1000; // wait for wifi to start
	public static final int UPDATE_DEFAULT = 600000; // default time between uploading data

	// This is the object that receives interactions from clients.
    private final IBinder mBinder = new LocalBinder();
	
	// URLs
    public static final String uploadURL = "https://cspc167.cs.man.ac.uk/~wireless/experiment/upload/upload.php";
	
    // preferences
    public static final String PREFS_NAME = "MyPrefsFile";
	private String myUsername;

	//private LinkedList<Message> newMessageCache;
	private static final String UNKNOWN = "";
	
	
	// DATA IS SPLIT INTO MORE PERMINENT STORAGE AND TEMP STORAGE TO SAVE MEMORY
	// perm data storage	
		public static final String BLUETOOTH_DATA_FILE = "bluetoothDataFile";
		public static final String WIFI_DATA_FILE = "wifiDataFile";
		public static final String GENERIC_DATA_FILE = "genericDataFile";
		public static final String SENSOR_DATA_FILE = "sensorDataFile";
		public static final String LOG_FILE = "logDataFile"; // UPLOADING WHOLE SYSTEM LOGS IS DISABLED FOR THE MOMENT
		public static final String QUESTION_DATA_FILE = "questionDataFile";
		
	// data temp stores
		private volatile Queue<String> tempGenericDataStorage;
		private volatile Queue<String> tempSensorDataStorage;
		private volatile Queue<String> tempBluetoothDevicesStorage;
		private volatile Queue<String> tempWiFiDevicesStorage;	 
		// no question temp store
		
    // GPS
	    private LocationManager GPSManager;
	    private String currentLocation;		
	    
	// Bluetooth scanning
		private String myAddress;
		//private BluetoothAdapter mBtAdapter;   
	    private BluetoothScanner bluetoothRunner;
	
	// WiFi
	    WiFiScanner wifiRunner;
	    
	// Sys logs
	//    private LogGatherer logging;
	    
    // uploader and/or storage class
    	private Upload upload;
    		
	// other sensors
		private Sensors sensorsRunner;
		
	// QUESTIONS
		private AskQuestions questionsRunner;
		
	// GSM & generic
		private int gsmSignalStrength;
		private int gsmLac;
		private int gsmCellID;
		GsmCellLocation location;			
    	private GenericScanner genericRunner;
	    private int cdmaDbm;
		private int cdmaEcio;
		private int evdoDbm;
		private int evdoSnr;
		private int gsmBitErrorRate;
		//private long scan_num;
		protected boolean batteryPresent;
		protected int batteryHealth;
		protected int batteryPlugged;
		protected int batteryVoltage;
		protected int batteryTemperature;
		protected String batteryTechnology;
		protected String batteryStatusString;
		protected String batteryHealthString;
		protected String batteryAcString;
		protected String batteryLevel;

		

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
	
    public class LocalBinder extends Binder {
        public McrScienceExperimentService getService() {
            return McrScienceExperimentService.this;
        }        
    }

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	Log.i("McrScienceExperiment", "Service start command issued");

		PowerManager.WakeLock lock=getLock(this.getApplicationContext());

	    if (!lock.isHeld() || (flags & START_FLAG_REDELIVERY) != 0) {
	      lock.acquire();
	      Log.i("McrScienceExperiment", "Acquiring wakelock");
	    }
		
			
    	
    	//Log.i("ManchesterWireless", "does it ever start?");
    	//The intent to launch when the user clicks the expanded notification
    	//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	//intent.setAction(Intent.ACTION_MAIN);
    	
    	//PendingIntent pendIntent = PendingIntent.getActivity(this, 0, intent, 0);

    	NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(this)
		        .setSmallIcon(R.drawable.mcrwireless)
		        .setContentTitle("Manchester Wireless Experiment")
		        .setContentText("The experiment is running.");
	
		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		
        Intent notificationIntent = new Intent(this.getApplicationContext(), McrScienceExperiment.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pendIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
    	
		mBuilder.setContentIntent(pendIntent);
		//NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		Notification notice = mBuilder.build();

    	notice.flags |= Notification.FLAG_NO_CLEAR;
    	this.startForegroundCompat(NOTIFICATION, notice);
    
    	
    	
    	
        return START_STICKY;
    }
    

    private static final Class<?>[] mSetForegroundSignature = new Class[] {
        boolean.class};
    private static final Class<?>[] mStartForegroundSignature = new Class[] {
        int.class, Notification.class};
    private static final Class<?>[] mStopForegroundSignature = new Class[] {
        boolean.class};

    private NotificationManager mNM;
    private int NOTIFICATION = R.string.local_service_started;

    private Method mSetForeground;
    private Method mStartForeground;
    private Method mStopForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Object[] mStartForegroundArgs = new Object[2];
    private Object[] mStopForegroundArgs = new Object[1];
	
    /**
     * Invoke a method using reflection
     * 
     * @param method
     * @param args
     */
    void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(this, args);
        } catch (InvocationTargetException e) {
            // Should not happen.
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalAccessException e) {
            // Should not happen.
            Log.w("ApiDemos", "Unable to invoke method", e);
        }
    }

    /**
     * This is a wrapper around the new startForeground method, using the older
     * APIs if it is not available.
     */
    void startForegroundCompat(int id, Notification notification) {
        // If we have the new startForeground API, then use it.
        if (mStartForeground != null) {
            mStartForegroundArgs[0] = Integer.valueOf(id);
            mStartForegroundArgs[1] = notification;
            invokeMethod(mStartForeground, mStartForegroundArgs);
            return;
        }

        // Fall back on the old API.
        mSetForegroundArgs[0] = Boolean.TRUE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
        mNM.notify(id, notification);
    }

    /**
     * This is a wrapper around the new stopForeground method, using the older
     * APIs if it is not available. (what what version does this not exist?)
     */
    void stopForegroundCompat(int id) {
        // If we have the new stopForeground API, then use it.
        if (mStopForeground != null) {
            mStopForegroundArgs[0] = Boolean.TRUE;
            invokeMethod(mStopForeground, mStopForegroundArgs);
            return;
        }

        // Fall back on the old API.  Note to cancel BEFORE changing the
        // foreground state, since we could be killed at that point.
        mNM.cancel(id);
        mSetForegroundArgs[0] = Boolean.FALSE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
    }

    private static String WAKELOCK_NAME = "MyWakeLock";
    
    private static volatile PowerManager.WakeLock lockStatic=null;

    /****    
     * Wakelock manager methods taken from 
     * https://github.com/commonsguy/cwac-wakeful/blob/master/src/com/commonsware/cwac/wakeful/WakefulIntentService.java
     */
    synchronized private static PowerManager.WakeLock getLock(Context context) {
      if (lockStatic == null) {
        PowerManager mgr=
            (PowerManager)context.getSystemService(Context.POWER_SERVICE);

        lockStatic=mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_NAME);
        lockStatic.setReferenceCounted(true);
      }

      return(lockStatic);
    }

    
    /***
     * In the current application design a new service is started each time the application first starts
     * and permission has been given to scan or a user chooses to turn scanning back on. 
     * So it's OK to start the experiment here with startAllScanners() etc. 
     * 
     */
	@Override
    public void onCreate() {
		Log.i("McrScienceExperiment", "New service created and experiment started");
		

		    
		
		mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
	    try {
	    	//TODO what does this bit do~?
	        mStartForeground = getClass().getMethod("startForeground",
	                mStartForegroundSignature);
	        mStopForeground = getClass().getMethod("stopForeground",
	                mStopForegroundSignature);
	        
	        
	    	startAllScanners();
	    	startUploadOperation();
	    	
	        return;
	    } catch (NoSuchMethodException e) {
	        // Running on an older platform.
	        mStartForeground = mStopForeground = null;
	    }
	    try {
	        mSetForeground = getClass().getMethod("setForeground",
	                mSetForegroundSignature);
	    } catch (NoSuchMethodException e) {
	        throw new IllegalStateException(
	                "OS doesn't have Service.startForeground OR Service.setForeground!");
	    }   
	}

	private void startAllScanners() {
		Log.i("McrScienceExperiment", "Service starts all scanners");
		
		// set some variables which threads need from the initial intent
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    this.setMyUsername(settings.getString("username", UNKNOWN)); 
	    this.setOnline(settings.getBoolean("online",true));
	    this.setUpdateInterval(settings.getLong("interval",McrScienceExperimentService.UPDATE_DEFAULT));
	    	    
        // populate variables for the scan
    	try {
			setSOFTWARE_VERSION(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
		} catch (NameNotFoundException e) {
			setSOFTWARE_VERSION(0); // 0 is reserved for errors;
		}
		
		// Initialize array adapters for newly discovered devices
        tempBluetoothDevicesStorage = new LinkedList <String> ();
        tempWiFiDevicesStorage = new LinkedList <String> ();
        tempGenericDataStorage = new LinkedList <String> ();
        tempSensorDataStorage = new LinkedList <String> ();
        //callList = new ArrayList<String>();
           
        // set up the LocationManager
        GPSManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		// Start asking questions
    	startAskingQuestions();
		// Start GPS
    	gpsStartListening();
    	// Start sensor monitor
    	startSensorsListening();
    	// Start Bluetooth
    	startGenericScanOperation();
    	// start gathering the android logs
    	//startLogCollecting();	    	
    	// Start Bluetooth
    	startBluetoothScanOperation();
    	// start wifi	    	
    	startWiFiScanOperation();
        
        /* Update the listener, and start it */
	    MyPhoneStateListener MyListener   = new MyPhoneStateListener();
        TelephonyManager Tel       = ( TelephonyManager )getSystemService(Context.TELEPHONY_SERVICE);
        Tel.listen(MyListener ,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        Tel.listen(MyListener ,PhoneStateListener.LISTEN_DATA_ACTIVITY);
        Tel.listen(MyListener ,PhoneStateListener.LISTEN_CELL_LOCATION);
        //Tel.listen(MyListener ,PhoneStateListener.LISTEN_CALL_STATE); not bothered about call state ow
        Tel.listen(MyListener ,PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
        Tel.listen(MyListener ,PhoneStateListener.LISTEN_SERVICE_STATE );
        
        // battery level listeners 
	    IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
	    registerReceiver(batteryLevelReceiver, batteryLevelFilter);   
	}

	@Override
	public void onDestroy() {
		Log.i("McrScienceExperiment", "Service destroyed");
		
    	super.onDestroy();
    	stopScanning();
    	// Make sure our notification is gone.
        stopForegroundCompat(NOTIFICATION);
    }
	
	public void setOnline(boolean online) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
	    editor.putBoolean("online", online);
	    // Commit the edits!
	    editor.commit();
	}

	public boolean getOnline() {
		 SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	     return settings.getBoolean("online", false);		
	}
		
    public void stopScanning() {
    	Log.i("McrScienceExperiment", "Service stops all scanning");
		
    	// STOP RECEIVERS
    	try {
	    	this.unregisterReceiver(batteryLevelReceiver);      
    	} 
    	catch(IllegalArgumentException e) {
    		// receiver already unregistered
    	}
    	try {
    		if(wifiRunner != null)
        		this.unregisterReceiver(wifiRunner.wifiReceiver);  
    	} 
    	catch(IllegalArgumentException e) {
    		// receiver already unregistered
    	}
    	try {
    		if(bluetoothRunner != null)
        		this.unregisterReceiver(bluetoothRunner.bluetoothReceiver);  
    	} 
    	catch(IllegalArgumentException e) {
    		// receiver already unregistered
    	}
    	
    	// stop logging, included in each method is storing of cached data
    	// stop logging (logging saves straight to a file anyway so we wont lose this)
    	//stopLogCollecting();
    	// Stop GPS
    	gpsStopListening();
    	// stop bluetooth
    	stopBluetoothScanOperation();
    	// stop wifi
    	stopWiFiScanOperation();
    	// stop generic
    	stopGenericScanOperation();
    	// stop sensors
    	stopSensorsListening();   	
    	// Stop upload
    	stopUploadOperation();
		// Stop asking questions
    	stopAskingQuestions();
    	

    	PowerManager.WakeLock lock=getLock(this.getApplicationContext());
        
        if (lock.isHeld()) {
        	Log.i("McrScienceExperiment", "releasing wakelock");
        	lock.release();
        }
    }
	 
    /*
    public synchronized void incScanNum() {
       	scan_num++;
	      SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putString("scan_num", Long.toString(scan_num));

	      // Commit the edits!
	      editor.commit();
    }
       
    public synchronized long getScanNum() {
       	return scan_num;
    }
    
   private synchronized void setScanNum(long l) {
       	scan_num = l;
    }
    */
    
	 //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	 // setters getters and incs
	 //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX	 
		public void setSOFTWARE_VERSION(int sOFTWARE_VERSION) {
			SOFTWARE_VERSION = sOFTWARE_VERSION;
		}

		public int getSOFTWARE_VERSION() {
			return SOFTWARE_VERSION;
		}
	 
		private void setSignalStrength(int sig) {
			gsmSignalStrength = sig;
		}
		
		private void setCdmaDbm(int sig) {
			cdmaDbm = sig;
		}
		   
		private void setCdmaEcio(int sig) {
			cdmaEcio = sig;
		}   
	   
		private void setEvdoDbm(int sig) {
			evdoDbm = sig;
		}
	   
		private void setEvdoSnr(int sig) {
			evdoSnr = sig;
		}
	    
		private void setGsmBitErrorRate(int sig) {
			gsmBitErrorRate = sig;
		}
		
		public int getSignalStrength() {
			return gsmSignalStrength;
		}

		public int getLac() {
			return gsmLac;
		}
		public void setLac(int lac) {
			gsmLac = lac;
		}

	    public void setGsmCellID(int gsmCellID) {
			this.gsmCellID = gsmCellID;
		}

		public int getGsmCellID() {
			return gsmCellID;
		}
		
		private void setMyUsername(String myUsername) {
			this.myUsername = myUsername;
		}

		public String getMyUsername() {
			return myUsername;
		}
		
		private int getCdmaDbm() {
			return cdmaDbm;
		}

		private int getCdmaEcio() {
			return cdmaEcio;
		}

		private int getEvdoDbm() {
			return evdoDbm;
		}

		private int getEvdoSnr() {
			return evdoSnr;
		}

		private int getGsmBitErrorRate() {
			return gsmBitErrorRate;
		}

		/*****************
	     * 
	     *  GPS
	     *  
	     *  
	     *****************/
	    
		public void setCurrentLocation(String currentLocation) {
			this.currentLocation = currentLocation;
		}

		public String getCurrentLocation() {
			return currentLocation;
		}
		
		
	    /**********************************************************************
	     * helpers for starting/stopping monitoring of GPS changes below
	     * GPS isn't in its own thread because it works best (and is more 
	     * efficient) if left on (See the Internet).
	     **********************************************************************/
	    private void gpsStartListening() {
	        GPSManager.requestLocationUpdates(
	                LocationManager.GPS_PROVIDER, 
	                GPS_SCAN_INTERVAL,  
	                0,  
	                this
	        );
	    }

	    private void gpsStopListening() {
	        if (GPSManager != null) {
	        		//System.out.println("STOPPING GPS");
	                GPSManager.removeUpdates(this);
	        }
	    }
	    
	    @Override
		public void onLocationChanged(Location location) {
	        // we got new location info. lets display it in the textview
	    	setCurrentLocation("Time: " + location.getTime() + "," +
	    			"Latitude:  " + location.getLatitude()  + "," +
	    			"Longitude: " + location.getLongitude() + "," +
	    			"Accuracy:  " + location.getAccuracy());
	       // System.out.println("GPS result: " + getCurrentLocation());
	    }    

	    @Override
		public void onProviderDisabled(String provider) {}    

	    @Override
		public void onProviderEnabled(String provider) {}    

	    @Override
		public void onStatusChanged(String provider, int status, Bundle extras) {}
		
	    
	    
	    /*****************
	     * 
	     *  Questions
	     *  
	     *  
	     *****************/
	    private void startAskingQuestions() {
	    	if(questionsRunner == null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.startthread");
	            questionsRunner = new AskQuestions(this);
	            questionsRunner.start();
	        }
	    }

	    //TODO does this just remove the question notification?
	    private void stopAskingQuestions() {
	    	if(questionsRunner != null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.stopthread");
	            (questionsRunner).requestStop();
	            Thread moribund = questionsRunner;
	            questionsRunner = null;
	            if(moribund != null) {
		            moribund.interrupt();
		            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "interrupted?" + moribund.isInterrupted());
	            }
	    	}	    	
	    }
	    
	    
	    
	    /*****************
	     * 
	     *  Sensors
	     *  
	     *  
	     *****************/
	    private void startSensorsListening() {
			if(sensorsRunner == null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.startthread");
	            sensorsRunner = new Sensors(this);
	            sensorsRunner.start();
	        }
	    }
    
	    public synchronized void stopSensorsListening() {
	    	if(sensorsRunner != null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.stopthread");
	            (sensorsRunner).requestStop();
	            Thread moribund = sensorsRunner;
	            sensorsRunner = null;
	            if(moribund != null) {
		            moribund.interrupt();
		            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "interrupted?" + moribund.isInterrupted());
	            }
	    	}
	    	// Stop sensor Scans
	    	String sensorData = getTempData(DataTypes.SENSOR_DATA);
			if(sensorData.length() > 0)
	  			addCacheData(DataTypes.SENSOR_DATA, sensorData);
    	}
	    



	 
	    /*****************
	     * 
	     *  Bluetooth
	     *  
	     *  
	     *****************/	    
	    public synchronized void setBluetoothAddress(String address) {
	    	this.myAddress = address;
	        //System.out.println("Bluetooth Address #3: " + myAddress);
	    }
	    
	    public synchronized String getBluetoothAddress() {
	    	return myAddress;
	    }
	 
	    public synchronized void startBluetoothScanOperation() {
	    	
	        if(bluetoothRunner == null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.startthread");         
	            bluetoothRunner = new BluetoothScanner(this);
	            

	            
	            
	            bluetoothRunner.start();
	       }
	    }
	    
	    public synchronized  void stopBluetoothScanOperation() {
	    	if(bluetoothRunner != null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.stopthread");
	           	bluetoothRunner.mBtAdapter.disable();
	            
	            (bluetoothRunner).requestStop();
	            Thread moribund = bluetoothRunner;
	            bluetoothRunner = null;
	            if(moribund != null) {
		            moribund.interrupt();
		            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "interrupted?" + moribund.isInterrupted());
	            }
	    	}
	    	// Stop Bluetooth
			String bluetoothData = getTempData(DataTypes.BLUETOOTH_DATA);
			//System.out.println("ONLINE: ATTEMPTING TO UPLOAD " + bluetoothData.length() + " THINGS");			  			
			if(bluetoothData.length() > 0)
				addCacheData(DataTypes.BLUETOOTH_DATA, bluetoothData);
	    }

	 
	    /*****************
	     * 
	     *  WiFi
	     *  
	     *  
	     *****************/
	 
	    public synchronized void startWiFiScanOperation() {
	        if(wifiRunner == null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.startthread");
	            WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
	            
		    	ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);		    	
		     	
	            wifiRunner = new WiFiScanner(this, wifi, connManager);
	            wifiRunner.start();
	        }
	    }
	    
	    public synchronized  void stopWiFiScanOperation() {
	    	if(wifiRunner != null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.stopthread");
	           	 
	            (wifiRunner).requestStop();
	            Thread moribund = wifiRunner;
	            wifiRunner = null;
	            if(moribund != null) {
		            moribund.interrupt();
		            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "interrupted?" + moribund.isInterrupted());
	            }
	    	}
	    	// stop wifi
	    	String wifiData = getTempData(DataTypes.WIFI_DATA);
			//System.out.println("ONLINE: ATTEMPTING TO UPLOAD " + bluetoothData.length() + " THINGS");			  			
			if(wifiData.length() > 0)
	  			addCacheData(DataTypes.WIFI_DATA, wifiData);
	    }


	    /*****************
	     * 
	     *  Uploading data to THE INTERNET!
	     *  
	     *  
	     *****************/

		public void setUpdateInterval(long updateInterval) {
			this.interval = updateInterval;			
		}
		
		public long getUpdateInterval() {
			return this.interval;
		}

	    public synchronized void startUploadOperation() {
	        if(upload == null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "Upload.startthread");         
	            upload = new Upload(this);	            
	            upload.start();
	        }
	    }
	    
	    public synchronized  void stopUploadOperation() {
	    	if(upload != null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "Upload.stopthread");
	            (upload).requestStop();
	            Thread moribund = upload;
	            upload = null;
	            if(moribund != null) {
		            moribund.interrupt();
		            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "interrupted?" + moribund.isInterrupted());
	            }
	    	}
	    }
		
	    /*****************
	     * 
	     *  Generic Data
	     *  This portion of this class includes listeners that will change the value of 
	     *  some fields when phone states change. e.g. when battery "health" changes then
	     *   batteryHealth = intent.getIntExtra("health", 0);
	     *  
	     *****************/
	    
	    public synchronized void startGenericScanOperation() {
	        if(genericRunner == null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.startthread");
	            genericRunner = new GenericScanner(this);
	            genericRunner.start();
	        }
	    }
	    
	    public synchronized  void stopGenericScanOperation() {
	    	if(genericRunner != null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "DoScan.stopthread");
	            (genericRunner).requestStop();
	            Thread moribund = genericRunner;
	            genericRunner = null;
	            if(moribund != null) {
		            moribund.interrupt();
		            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "interrupted?" + moribund.isInterrupted());
	            }
	    	}
	    	// Stop Generic Scans
	    	String genericData = getTempData(DataTypes.GENERIC_DATA);
			//System.out.println("ONLINE: ATTEMPTING TO UPLOAD " + bluetoothData.length() + " THINGS");			  			
			if(genericData.length() > 0)
	  			addCacheData(DataTypes.GENERIC_DATA, genericData);
	    }
	       
	    /**
	     * Computes the battery level by registering a receiver to the intent triggered 
	     * by a battery status/level change.
	     */
	      BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
	
				@Override
				public void onReceive(Context context, Intent intent) {
	          	
		                int rawlevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
		                int level = -1;
		                if (rawlevel >= 0 && scale > 0) {
		                    level = (rawlevel * 100) / scale;
		                }
		                batteryLevel = Integer.toString(level);
		                
		                int status = intent.getIntExtra("status", BatteryManager.BATTERY_STATUS_UNKNOWN);
		                //String newPhoneState = intent.getStringExtra("status");
	
		                Log.v("McrScienceExperiment", "BATTERY_STATE_CHANGED:"+status+":");
	                  
		                batteryHealth = intent.getIntExtra("health", 0);
		                batteryPresent = intent.getBooleanExtra("present",false);
		                batteryPlugged = intent.getIntExtra("plugged", 0);
		                batteryVoltage = intent.getIntExtra("voltage", 0);
		                batteryTemperature = intent.getIntExtra("temperature",0);
		                batteryTechnology = intent.getStringExtra("technology");
		                
	
	                  switch (status) {
		                    case BatteryManager.BATTERY_STATUS_UNKNOWN:
		                        batteryStatusString = "unknown";
		                        break;
		                    case BatteryManager.BATTERY_STATUS_CHARGING:
		                    	batteryStatusString = "charging";
		                        break;
		                    case BatteryManager.BATTERY_STATUS_DISCHARGING:
		                    	batteryStatusString = "discharging";
		                        break;
		                    case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
		                    	batteryStatusString = "not charging";
		                        break;
		                    case BatteryManager.BATTERY_STATUS_FULL:
		                    	batteryStatusString = "full";
		                        break;
	                  }
	
	                  switch (batteryHealth) {
		                    case BatteryManager.BATTERY_HEALTH_UNKNOWN:
		                    	batteryHealthString = "unknown";
		                        break;
		                    case BatteryManager.BATTERY_HEALTH_GOOD:
		                    	batteryHealthString = "good";
		                        break;
		                    case BatteryManager.BATTERY_HEALTH_OVERHEAT:
		                    	batteryHealthString = "overheat";
		                        break;
		                    case BatteryManager.BATTERY_HEALTH_DEAD:
		                    	batteryHealthString = "dead";
		                        break;
		                    case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
		                    	batteryHealthString = "voltage";
		                        break;
		                    case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
		                    	batteryHealthString = "unspecified failure";
		                        break;
	                  }
	
	
	                  switch (batteryPlugged) {
		                    case BatteryManager.BATTERY_PLUGGED_AC:
		                    	batteryAcString = "plugged ac";
		                        break;
		                    case BatteryManager.BATTERY_PLUGGED_USB:
		                    	batteryAcString = "plugged usb";
		                        break;
		               }
	                  
		                		                
	            
	          }
	      };
	      
	      


		// GENERIC SCAN DATA
		public String getMobileData() {
			
			TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
			GsmCellLocation aLocation = (GsmCellLocation) tm.getCellLocation();

			String mobileData =  
			tm.getDeviceId() + "," +
			tm.getCallState() + "," +
			tm.getDataActivity() + "," +
			tm.getDataState() + "," +
			tm.getDeviceSoftwareVersion() + "," +
			tm.getLine1Number() + "," +
			tm.getNetworkCountryIso() + "," +
			tm.getNetworkOperator() + "," +
			tm.getNetworkOperatorName() + "," +
			tm.getNetworkType() + "," +
			tm.getPhoneType() + "," +
			tm.getSimCountryIso() + "," +
			tm.getSimOperator() + "," +
			tm.getSimOperatorName() + "," +
			tm.getSimSerialNumber() + "," +
			tm.getSimState() + "," +
			tm.getSubscriberId() + "," +
			tm.getVoiceMailAlphaTag() + "," +
			tm.getVoiceMailNumber() + "," +
			//These last two seem to be repeated from the method that calls this one
			aLocation.getCid() + "," +
			aLocation.getLac() + ",";
			
			List<NeighboringCellInfo> neighboringcells = tm.getNeighboringCellInfo();
			
			for(int i = 0; i < neighboringcells.size(); i++) {
				mobileData += "NEIGHBORINGCELL" + "," +
				 i + "," +
				 neighboringcells.get(i).getCid() + "," +
				 neighboringcells.get(i).getLac() + "," +
				 neighboringcells.get(i).getNetworkType() + "," +
				 neighboringcells.get(i).getPsc() + "," +
				 neighboringcells.get(i).getRssi() + ",";
			}
			 
			return mobileData;
			
		}
		
		public String getGenericData() {
		
			return
			getLac() + "," + 
			getGsmCellID() + "," + 
			getMobileData() + "," +
			batteryLevel + "," + 
			batteryStatusString + "," +
        	batteryHealthString + "," +
        	batteryAcString + "," +
			batteryPresent + "," +
			batteryPlugged + "," +
			batteryVoltage + "," +
			batteryTemperature + "," +
			batteryTechnology + "," +
			getSignalStrength() + "," + 
			getCdmaDbm() + "," + 
			getCdmaEcio() + "," + 
			getEvdoDbm() + "," + 
			getEvdoSnr() + "," + 
			getGsmBitErrorRate() + "," + 
			getSOFTWARE_VERSION() + "," +
			getCurrentLocation();
		}
	    

	    
	    /*********************************/
	    /* Start the PhoneState listener */
		/* Commented lots of stuff out that we're not logging (at the moment) */
		/*********************************/
		
	    private class MyPhoneStateListener extends PhoneStateListener
	    {
	    	
	    	/*
	    	@Override
			public void onCallStateChanged(int state, String incomingNumber) {
	    		
	    		try {
		    		Log.i("McrScienceExperiment", "CALL STATE CHANGED");	    		
		    		if(state == TelephonyManager.CALL_STATE_IDLE) {
			    	    	Log.i("McrScienceExperiment", "CALL_STATE_IDLE:"+incomingNumber+":");
			   		}
		    		else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
			    	    	Log.i("McrScienceExperiment", "CALL_STATE_OFFHOOK:"+incomingNumber+":");
			   		}
		    		else if(state == TelephonyManager.CALL_STATE_RINGING) {
			    	    	Log.i("McrScienceExperiment", "CALL_STATE_RINGING:"+incomingNumber+":");
			   		}
		    		else {
		    			Log.i("McrScienceExperiment", "DEFAULT:"+incomingNumber+":");
			  	    }
	    		}
	    		catch (Exception e) {
	    			e.printStackTrace();
	    		}

			}
			*/
	    	
			/* Get the Signal strength from the provider, each tiome there is an update */
			@Override
			public void onSignalStrengthsChanged(SignalStrength signalStrength)
			{
			   super.onSignalStrengthsChanged(signalStrength);
					   
			   setSignalStrength(signalStrength.getGsmSignalStrength());
			   setCdmaDbm(signalStrength.getCdmaDbm());
			   setCdmaEcio(signalStrength.getCdmaEcio());
			   setEvdoDbm(signalStrength.getEvdoDbm());
			   setEvdoSnr(signalStrength.getEvdoSnr());
			   setGsmBitErrorRate(signalStrength.getGsmBitErrorRate());
			   
			   /*
			   Log.i("McrScienceExperiment", "Signal Strengths Changed:"+
						signalStrength.getGsmSignalStrength()+":"+
						signalStrength.getCdmaDbm()+":"+
						signalStrength.getCdmaEcio()+":"+
						signalStrength.getEvdoDbm()+":"+
						signalStrength.getEvdoSnr()+":"+
						signalStrength.getGsmBitErrorRate()+":");
				*/
			}

			@Override
			public void onCellLocationChanged(CellLocation aLocation) {
				super.onCellLocationChanged(aLocation);
				location = (GsmCellLocation) aLocation;
				setLac(location.getLac());
				setGsmCellID(location.getCid());
				//Log.i("McrScienceExperiment", "CELL LOC CHANGED:"+location.getLac()+":"+location.getCid()+"");	
			}
			
			/* 
			@Override
			public void onMessageWaitingIndicatorChanged(boolean mwi) {
				super.onMessageWaitingIndicatorChanged(mwi);
				Log.i("McrScienceExperiment", "MESSAGE WAITING:"+mwi+":");
			}
			
			@Override
			public void onCallForwardingIndicatorChanged(boolean cfi){
				super.onCallForwardingIndicatorChanged(cfi);
				Log.i("McrScienceExperiment", "CALL FORWARDING INDICATION CHANGES:"+cfi+":");
			}
		
			@Override
			public void onDataActivity(int direction){
				super.onDataActivity(direction);
				Log.i("McrScienceExperiment", "DATA ACTIVITY DIRECTION:"+direction+":");
			}
			
			@Override
			public void onDataConnectionStateChanged(int state) {
				super.onDataConnectionStateChanged(state);
				Log.i("McrScienceExperiment", "DATA CONNECTION STATE:"+state+":");
			}
			
			@Override
			public void onDataConnectionStateChanged(int state, int networkType) {
				super.onDataConnectionStateChanged(state, networkType);
				Log.i("McrScienceExperiment", "DATA CONNECTION TYPE STATE:"+state+":"+networkType+":");
			}
			
			@Override
			public void onServiceStateChanged(ServiceState serviceState) {
				super.onServiceStateChanged(serviceState);
				Log.i("McrScienceExperiment", "SERVICE STATE:"+serviceState.getOperatorAlphaLong()+":"+
	    	    		  								serviceState.getOperatorAlphaShort()+":"+
	    	    		  								serviceState.getOperatorNumeric()+":"+
	    	    		  								serviceState.getState()+":"+
	    	    		  								serviceState.getIsManualSelection()+":"+
	    	    		  								serviceState.getRoaming()+":");
			}
			*/
		}
		
	    /*****************
	     * 
	     *  Log Data
	     *  
	     *  
	     *****************/
	    /*
	    public synchronized void startLogCollecting() {
	        if(logging == null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "Upload.startthread");         
	            logging = new LogGatherer(this);
	            logging.start();
	        }
	    }
	    
	    public synchronized  void stopLogCollecting() {
	    	if(logging != null){
	            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "Upload.stopthread");
	            (logging).requestStop();
	            Thread moribund = logging;
	            logging = null;
	            if(moribund != null) {
		            moribund.interrupt();
		            android.util.Log.v("@@@@@@@@@@@@@@@@@@@@", "interrupted?" + moribund.isInterrupted());
	            }
	    	}
	    }	   
		*/
	    
		/*****************
	     * 
	     *  Shared methods for temp caches and files
	     *  
	     *  
	     *****************/ 
		public synchronized void addTempData(String data, DataTypes TYPE) {
			Log.v("McrScienceExperiment", "Adding temp data for "+TYPE.toString()+" current size is="+getTempDataSize(TYPE));
            
			Queue<String> cache = null;
			if(TYPE == DataTypes.SENSOR_DATA)
				cache = tempSensorDataStorage;
			else if(TYPE == DataTypes.GENERIC_DATA)
				cache = tempGenericDataStorage;
			else if(TYPE == DataTypes.WIFI_DATA)
				cache = tempWiFiDevicesStorage;
			else if(TYPE == DataTypes.BLUETOOTH_DATA)
				cache = tempBluetoothDevicesStorage;
			else
				return;
			
			if(getTempDataSize(TYPE) > MAX_TEMP_BUFFER_SIZE) {
				Log.v("McrScienceExperiment", "Cache for "+TYPE.toString()+ " bigger than "+Integer.toString(MAX_TEMP_BUFFER_SIZE));
	            
		    	String someData = getTempData(TYPE);
	  			if(someData.length() > 0) {
	  				addCacheData(TYPE, someData);
	  				 
	  			}
			}
			Calendar c = Calendar.getInstance();
			cache.add(c.getTime().toGMTString()+","+data);
		}
		
		public synchronized String getTempData(DataTypes type) {
			
			Queue<String> cache = null;
			if(type == DataTypes.SENSOR_DATA)
				cache = tempSensorDataStorage;
			else if(type == DataTypes.GENERIC_DATA)
				cache = tempGenericDataStorage;
			else if(type == DataTypes.WIFI_DATA)
				cache = tempWiFiDevicesStorage;
			else if(type == DataTypes.BLUETOOTH_DATA)
				cache = tempBluetoothDevicesStorage;
			else
				return "";
			
			String data = new String();
			while(cache.size() > 0 )
				data += cache.remove()+"\n"; 
			return data;
		}

		public int getTempDataSize(DataTypes TYPE) {
			
			Queue<String> cache = null;
			if(TYPE == DataTypes.SENSOR_DATA)
				cache = tempSensorDataStorage;
			else if(TYPE == DataTypes.GENERIC_DATA)
				cache = tempGenericDataStorage;
			else if(TYPE == DataTypes.WIFI_DATA)
				cache = tempWiFiDevicesStorage;
			else if(TYPE == DataTypes.BLUETOOTH_DATA)
				cache = tempBluetoothDevicesStorage;
			else
				return 0;
			
			if(cache != null)
				return cache.size();
			else
				return 0;
		}

		public String getCachedData(DataTypes TYPE) {

			String outputFile = null;
			if(TYPE == DataTypes.SENSOR_DATA) {
				outputFile = McrScienceExperimentService.SENSOR_DATA_FILE;
			}
			else if(TYPE == DataTypes.BLUETOOTH_DATA) {
				outputFile = McrScienceExperimentService.BLUETOOTH_DATA_FILE;
			}
			else if(TYPE == DataTypes.GENERIC_DATA) {
				outputFile = McrScienceExperimentService.GENERIC_DATA_FILE;
			}
			else if(TYPE == DataTypes.WIFI_DATA) {
				outputFile = McrScienceExperimentService.WIFI_DATA_FILE;
			}
			else if(TYPE == DataTypes.LOG_DATA) {
				outputFile = McrScienceExperimentService.LOG_FILE;
			}
			else if(TYPE == DataTypes.QUESTION_DATA) {
				outputFile= McrScienceExperimentService.QUESTION_DATA_FILE;
			}
			
			StringBuilder text = new StringBuilder();
		    Scanner scanner = null;
		    FileInputStream fIn = null;
			try {
				fIn = openFileInput(outputFile);
				scanner = new Scanner(fIn);
			
				while (scanner.hasNextLine()){
					text.append(scanner.nextLine());
				}
		    }
		    catch (FileNotFoundException e) {
		    	// Sometimes you'll try to get a cache file which hasn't been created yet. This shouldn't be a problem, will just return an empty string.
				//e.printStackTrace();
			}
		    finally{
		    	if(scanner != null)
		    		scanner.close();
		    	if(fIn != null) {
					try {
						fIn.close();
					} catch (IOException e) {
						//e.printStackTrace();
					}
		    	}
		    }
		    return text.toString();
		}
		
		public boolean deleteCachedData(DataTypes TYPE) {
			
			String outputFile = null;
			if(TYPE == DataTypes.SENSOR_DATA) {
				outputFile = McrScienceExperimentService.SENSOR_DATA_FILE;
			}
			else if(TYPE == DataTypes.BLUETOOTH_DATA) {
				outputFile = McrScienceExperimentService.BLUETOOTH_DATA_FILE;
			}
			else if(TYPE == DataTypes.GENERIC_DATA) {
				outputFile = McrScienceExperimentService.GENERIC_DATA_FILE;
			}
			else if(TYPE == DataTypes.WIFI_DATA) {
				outputFile = McrScienceExperimentService.WIFI_DATA_FILE;
			}
			else if(TYPE == DataTypes.LOG_DATA) {
				outputFile = McrScienceExperimentService.LOG_FILE;
			}
			else if(TYPE == DataTypes.QUESTION_DATA) {
				outputFile= McrScienceExperimentService.QUESTION_DATA_FILE;
			}
			
			File file = getFileStreamPath(outputFile);
			if (file.exists()) {
				file.delete();
			}
			try {
				file.createNewFile();
				return true;
			} 
			catch (IOException e) {				
				//e.printStackTrace();
				return false;
			}
		}
		
		/**
		 * Cache the data in files, returns true if successful and false if something went wrong of file is being deleted
		 */
		public boolean addCacheData(DataTypes TYPE, String receivedMessages) {
			
			
			String dataCacheFile = "";
		    boolean error = true;
			
			if(TYPE == DataTypes.BLUETOOTH_DATA) {
	        	dataCacheFile = McrScienceExperimentService.BLUETOOTH_DATA_FILE;
			}
			else if(TYPE == DataTypes.WIFI_DATA) {
	        	dataCacheFile = McrScienceExperimentService.WIFI_DATA_FILE;
			}
			else if(TYPE == DataTypes.SENSOR_DATA) {
	        	dataCacheFile = McrScienceExperimentService.SENSOR_DATA_FILE;
			}
			else if(TYPE == DataTypes.GENERIC_DATA) {
	        	dataCacheFile = McrScienceExperimentService.GENERIC_DATA_FILE;
			}
			else if(TYPE == DataTypes.LOG_DATA) {
	        	dataCacheFile = McrScienceExperimentService.LOG_FILE;
			}
			else if(TYPE == DataTypes.QUESTION_DATA) {
	        	dataCacheFile = McrScienceExperimentService.QUESTION_DATA_FILE;
			}
			else {
				return false;
			}
			    
			//System.out.println("################# Caching Data: " + receivedMessages);
			FileOutputStream fOut = null;

			try {
				
				File file = getFileStreamPath(dataCacheFile);
				Log.v("McrScienceExperiment", "Adding data to file cache for "+TYPE.toString()+ ". Current size is "+file.length());
				
				fOut = openFileOutput(dataCacheFile, Context.MODE_APPEND); 
				OutputStreamWriter osw = new OutputStreamWriter(fOut);  
				osw.write(receivedMessages);
				osw.flush(); 
				osw.close();
		    }
			catch (FileNotFoundException e) {
				//e.printStackTrace();
				// probably shouldnt happen unless we dont have permission to create files
				error = false;
			}
			catch (IOException e) {
				//e.printStackTrace();
				error = false;
			} 
			finally{
		    	if(fOut != null) {
					try {
						fOut.close();
					} catch (IOException e) {
						//e.printStackTrace();
						error = false;
					}
		    	}
		    }
			File file = getFileStreamPath(dataCacheFile);
			Log.v("McrScienceExperiment", "Adding data to file cache for "+TYPE.toString()+ ". Current size is "+file.length());
			return error;
		}
		
}
