package science.experiment;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;


public class BluetoothScanner extends Thread {
	
  	 // Must be volatile:
      private volatile boolean stop = false;
     // private volatile int scan_num = 0;
      McrScienceExperimentService mcrScience;
      
      BluetoothAdapter mBtAdapter;
      private String bluetoothAddress;
      
      
      // Other variables for things created in this scan
     
      public BluetoothScanner(McrScienceExperimentService mcrScienceExperimentService) {
    	  mcrScience = mcrScienceExperimentService;
    	  mBtAdapter = BluetoothAdapter.getDefaultAdapter();
      }

      @Override
      public void run() {	
    	  while (!stop) {
	          doDiscovery();
	          try {
	        	  Thread.sleep(McrScienceExperimentService.SCIENCE_SCAN_TICK_RATE);
	          } catch (InterruptedException e) {
	        	  // TODO Auto-generated catch block
	        	  //e.printStackTrace();
	          }
	      }
      }

      public synchronized void requestStop() {
              stop = true;
              stopDiscovery();
      }

	   /**
	    * Start device discover with the BluetoothAdapter
	    */
	   void doDiscovery() {
	       //if (D) Log.d(TAG, "doDiscovery()");
	      
	       // Indicate scanning in the title
	       //setProgressBarIndeterminateVisibility(true);
	        // If BT is not on, request that it be enabled.
	        if (!mBtAdapter.isEnabled()) {
	        	mBtAdapter.enable();
	            // Set Bluetooth address
	        	bluetoothAddress = BluetoothAdapter.getDefaultAdapter().getAddress();
	            mcrScience.setBluetoothAddress(bluetoothAddress);
	            //System.out.println("Bluetooth Address #4: " + bluetoothAddress);
	        }
	        if(bluetoothAddress == null) {
	        	bluetoothAddress = BluetoothAdapter.getDefaultAdapter().getAddress();
	        	//System.out.println("Bluetooth Address #5: " + bluetoothAddress);
	        }
	        
			// If we're already discovering, stop it
			if (mBtAdapter.isDiscovering()) {
				mBtAdapter.cancelDiscovery();
			}
			
			try {
				mcrScience.unregisterReceiver(this.bluetoothReceiver);
			}
			catch (IllegalArgumentException e) {
				// receiver already unregistered
			}
			
			// Register for broadcasts when a bluetooth device is discovered
			IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
	        mcrScience.registerReceiver(this.bluetoothReceiver, filter);
 	  
	        // Register for broadcasts when bluetooth discovery has finished
	        IntentFilter filter2 = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
	        mcrScience.registerReceiver(this.bluetoothReceiver, filter2);
			
			// Request discover from BluetoothAdapter
	        Log.i("McrScienceExperiment", "Starting new Bluetooth scan");
    		
			mBtAdapter.startDiscovery();
	   }
	   
	   /**
	    * Start device discover with the BluetoothAdapter
	    */
	   private void stopDiscovery() {
	   		// stop the scanning	   		
		   try {
			   mcrScience.unregisterReceiver(this.bluetoothReceiver);
		   }
		   catch (IllegalArgumentException e) {
			   // receiver already unregistered
		   }
		   mBtAdapter.cancelDiscovery();
		   // return bluetooth chip to its starting state
		   mBtAdapter.disable();
	   }      
      
      // The BroadcastReceiver that listens for discovered devices and
      // changes the title when discovery is finished
      final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
      	@Override
          public void onReceive(Context context, Intent intent) {
              String action = intent.getAction();
              // When discovery finds a device
              if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                  // Get the BluetoothDevice object from the Intent
                  BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                  //mcrScience.addData(device.getName() + "\n" + device.getAddress());
                  mcrScience.addTempData(
                		 bluetoothAddress + "," + 
                		// mcrScience.getScanNum() + "," + 
                		//  mcrScience.getGenericData() + "," +
                		  device.getBluetoothClass() + "," +
                		  device.getAddress() + ","  + "\n", DataTypes.BLUETOOTH_DATA
                 );
                  Log.i("McrScienceExperiment", "Bluetooth found device");
              } 
              else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) { 
            	  Log.i("McrScienceExperiment", "Bluetooth discovery finished");
            	  try {
            		  mcrScience.unregisterReceiver(this);
            	  }
            	  catch (IllegalArgumentException e) {
            		  // receiver already unregistered
            	  }
            	  mBtAdapter.disable();
              }
          }
      };

      
}
