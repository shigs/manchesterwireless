package science.experiment;


import java.util.List;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;


public class WiFiScanner extends Thread  {
	
	private McrScienceExperimentService mcrScience;
	private volatile boolean stop = false;
 	WifiManager mWifiManager;
	private List<ScanResult> wireless;
	private ConnectivityManager connManager;
	
	private boolean initialWiFiStatus = false;
	
	public WiFiScanner(McrScienceExperimentService mcrScienceExperimentService, 
			WifiManager _mWifiManager, 
			ConnectivityManager _connManager) {

    	mcrScience = mcrScienceExperimentService;
    	// Setup WiFi
    	mWifiManager = _mWifiManager;    	
    	connManager = _connManager;
    	initialWiFiStatus = mWifiManager.isWifiEnabled();
    	


    	
    }
	
	/**
	 * The WifiManager seems to be in WIFI_UNKNOW state when you have the tethering enabled.
	 * Access points methods are marked as @hide in 2.2 so can't access them directly. Besides there is only one method (setWifiApEnabled) available on 2.2
	 * Can't use any wifi functionality when device is an access point
	 */
	public boolean isWifiApState() {
		int temp = mWifiManager.getWifiState();
		// This might look dumb but I don't know what states look like on newer versions of android
		// I'm not sure if mWifiManager.WIFI_STATE_UNKNOWN has changed and if there are more states now
		if(temp == mWifiManager.WIFI_STATE_DISABLED) {
			return false;
		}
		else if(temp == mWifiManager.WIFI_STATE_DISABLING) {
			return false;
		}
		else if(temp == mWifiManager.WIFI_STATE_ENABLED) {
			return false;
		}
		else if(temp == mWifiManager.WIFI_STATE_ENABLING) {
			return false;
		}
		else 
			return true;
	}
	
	
	@Override
	public void run() {
		
	      while (!stop && !isWifiApState()) {
	    	  	/* Some disruption is inevitable whilst running the app unless we 
	    	  	 * just turn WiFi on and be done with it. 
	    	  	 * Added some complexity to catch if wifi has been put into AP mode.
	    	  	 */
	    	  	while(!mWifiManager.isWifiEnabled() && !isWifiApState()) {
	    	  		mWifiManager.setWifiEnabled(true);
	    	  		try {
	    	  			// WiFi takes some seconds to start, causes errors if we start a scan too soon
	    	  			Thread.sleep(McrScienceExperimentService.WIFI_START_WAIT);
					} catch (InterruptedException e) {
						//e.printStackTrace();
					}
	    	  		finally {
	    	  			if(isWifiApState()) {
	    	  				stop = true;
	    	  	    		break;
	    	  	    	}
	    	  		}
	    	  	}
	  			if(isWifiApState()) {
	  				stop = true;
	  	    		break;
	  	    	}
	    	  		
	    		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	    	  	if(mWifi.isConnectedOrConnecting()) {
	    	  		//System.out.println("WIFI CONNECTED OR CONNECTING");
		    	  	WifiInfo info = mWifiManager.getConnectionInfo();
		    	  	String infoString = convertWiFiConnectionToString(mWifi,info);
	
		    	  	mcrScience.addTempData(
		            			 // mcrScience.getScanNum() + "," +
		            			  //mcrScience.getGenericData() + "," +
		            			  infoString +
		                		  "\n", DataTypes.WIFI_DATA
		            			  );
	    	  	}
	    	  
	            // Add wifi listeners
	    	  	// SCAN_RESULTS_AVAILABLE_ACTION might result in multiple scan results for the same time around the loop (about 500ms apart), but it allows us to get data from startScan faster
	    	  	// unregister receiver if there is one
				try {
					  mcrScience.unregisterReceiver(wifiReceiver);
				}
		    	catch (IllegalArgumentException e) {
		    		  // receiver already unregistered
		    	}
	    	  	
	    	  	IntentFilter wifiFilter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
	            mcrScience.registerReceiver(wifiReceiver, wifiFilter);
	            Log.i("McrScienceExperiment", "Starting new wifi scan");
	    		
	    	  	mWifiManager.startScan();
	    	  	
	            try {
					Thread.sleep(McrScienceExperimentService.WIFI_SCAN_TIME);
					
				} catch (InterruptedException e) {
					//e.printStackTrace();
				}
				// unregister receiver
				try {
					  mcrScience.unregisterReceiver(wifiReceiver);
				}
		    	catch (IllegalArgumentException e) {
		    		  // receiver already unregistered
		    	}
				/* After we've scan for a bit we can turn WiFi off (if it was off when we started).
				 * This may cause problems if user wants to turn WiFi on between the start 
				 * of the program and here.
				 */
				if(!initialWiFiStatus && !isWifiApState())
					mWifiManager.setWifiEnabled(false);
	    	  	try {
	    	  		if(initialWiFiStatus)
	    	  			Thread.sleep(McrScienceExperimentService.SCIENCE_SCAN_TICK_RATE-McrScienceExperimentService.WIFI_SCAN_TIME);
	    	  		else
	    	  			Thread.sleep(McrScienceExperimentService.SCIENCE_SCAN_TICK_RATE-McrScienceExperimentService.WIFI_SCAN_TIME-McrScienceExperimentService.WIFI_START_WAIT);
				} catch (InterruptedException e) {
					//e.printStackTrace();
				}
	      }
     }
	 
	/*
     public synchronized void incScanNum() {
       	scan_num++;
       }
       
    public synchronized int getScanNum() {
       	return scan_num;
    }
    */

     public synchronized void requestStop() {
         stop = true;
         try {
			  mcrScience.unregisterReceiver(wifiReceiver);
         }
         catch (IllegalArgumentException e) {
        	 // receiver already unregistered
         }
         if(!initialWiFiStatus && !isWifiApState())
        	 mWifiManager.setWifiEnabled(false);	  	
     }
     
     private String convertWiFiConnectionToString(NetworkInfo mWifi, WifiInfo info) {
		 String infoString = "";
		 
		 	infoString += info.getBSSID() + ",";
	  		infoString += info.getMacAddress() + ",";
	  		infoString += info.getSSID() + ",";
	  		infoString += info.getHiddenSSID() + ",";
	  		infoString += info.getIpAddress() + ",";
	  		infoString += info.getLinkSpeed() + ",";
	  		infoString += info.getNetworkId() + ",";
	  		infoString += info.getRssi() + ",";
	  		infoString += mWifi.getDetailedState() + ",";
	  		infoString += WifiInfo.getDetailedStateOf(info.getSupplicantState());
	  		
    	 return infoString;
     }
 
     
     
     private String convertResultToString(ScanResult result) {
		 String returnResult = String.format(" SSID: %s, RSSI: %s dBm, BSSID: %s, Capabilities: %s, Frequency: %s", 
				  result.SSID, result.level, result.BSSID, result.capabilities, result.frequency);
    	 return returnResult;
     }

	   
	// The BroadcastReceiver that listens for discovered devices and
	// changes the title when discovery is finished
	final BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context c, Intent i){
	    	//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ WiFi Networks Found");
	    	wireless =  mWifiManager.getScanResults(); // Returns a <list> of scanResults
	        try {
	        	for (ScanResult result : wireless) {
	        		mcrScience.addTempData(convertResultToString(result), DataTypes.WIFI_DATA);
	        	}
	        }
	        catch (NullPointerException e) {
	        	//e.printStackTrace();
	        }
	    }
	};
	
}
