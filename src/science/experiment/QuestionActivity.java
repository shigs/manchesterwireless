package science.experiment;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class QuestionActivity extends Activity {

	private Button okButton;
	private Button cancelButton;
	private QuestionActivity context;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
    }
    
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(context)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(R.string.cancel)
            .setMessage(R.string.yousure)
            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	NotificationManager mNotificationManager =
        			    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            	mNotificationManager.cancelAll();
                finish();    
            }

        })
        .setNegativeButton("No", null)
        .show();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.getApplicationContext();
        setContentView(R.layout.questions);
        
        // set cancel to close activity and delete notifications
        cancelButton = (Button)findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				//Ask the user if they want to quit
				new AlertDialog.Builder(context)
	            .setIcon(android.R.drawable.ic_dialog_alert)
	            .setTitle(R.string.cancel)
	            .setMessage(R.string.yousure)
	            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
	            {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		            	
		            	NotificationManager mNotificationManager =
		        			    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		            	mNotificationManager.cancelAll();
		                finish();    
		            }
	
		        })
		        .setNegativeButton("No", null)
		        .show();
			}
		});
		
        // set ok button to upload answers
        okButton = (Button)findViewById(R.id.ok);
        okButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// upload answers (Actually store them and let Upload do the rest...)
				// add question to quiestion store, bit of code duplication here but its probably easier than the alternatives
								
				// data is time,answer1,answer2
				Calendar c = Calendar.getInstance();
				EditText answer1box = (EditText)findViewById(R.id.answer1);
				EditText answer2box = (EditText)findViewById(R.id.answer2);
				String answer1 = answer1box.getText().toString();
				String answer2 = answer2box.getText().toString();
				
				String data = c.getTime().toGMTString()+","+answer1+","+answer2+"\n";
				FileOutputStream fOut = null;
				String dataCacheFile = McrScienceExperimentService.QUESTION_DATA_FILE;
				try {
					fOut = openFileOutput(dataCacheFile, Context.MODE_APPEND); 
					OutputStreamWriter osw = new OutputStreamWriter(fOut);  
					osw.write(data);
					osw.flush(); 
					osw.close();
					Log.i("questions", "Added data: "+data);
			    }
				catch (FileNotFoundException e) {
					//e.printStackTrace();
				}
				catch (IOException e) {
					//e.printStackTrace();
				} 
				finally{
			    	if(fOut != null) {
						try {
							fOut.close();
						} catch (IOException e) {
							//e.printStackTrace();
						}
			    	}
			    }
				// once we have stored the stuff we can exit. thanks.
				NotificationManager mNotificationManager =
        			    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            	mNotificationManager.cancelAll();
                finish();    
				
			}
		});
        
        
        // Set result CANCELED incase the user backs out
        setResult(Activity.RESULT_CANCELED);
    }    
    

	
}
